package com.example.calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var display: TextView
    private lateinit var history: TextView
    private var resultMode: Boolean = true
    private var zeroBtnBlocked: Boolean = true
    private var dotBtnBlocked: Boolean = false
    private var opInProgress: Boolean = false
    private var firstOp: Double = 0.0
    private var secondOp: Double = 0.0
    private var result: String? = null
    private var operator: String? = null
    private var opReady: Boolean = false
    private var opFinished: Boolean = false
    private var resultTemp: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.display = findViewById(R.id.result_txt)
        this.history = findViewById(R.id.history_txt)

        val clearBtn = findViewById<Button>(R.id.clear_btn)

        clearBtn.setOnClickListener{
            clear()
        }

    }


    fun keyBoard(v: View){
        val symbol: String

        symbol = this.getSymbolById(v.id)

        // verifica se a operação atual foi finalizada
        if(this.opFinished) {
            if (v.id == R.id.dot_btn || symbol[0].isDigit()) {
                this.clear()
            }else{
                this.resultTemp = this.result
                this.clear()
                this.display.text = this.resultTemp
            }
        }

        if(v.id != R.id.zero_btn) {

            // Se a tecla pressionada for um número ou ponto, o valor é adicionado ao display neste bloco
            if(symbol[0].isDigit() || (v.id == R.id.dot_btn && !this.dotBtnBlocked)){
                if(this.resultMode && (this.zeroBtnBlocked || !this.dotBtnBlocked)){
                    this.display.text = ""
                    this.resultMode = false         //sai do modo resultado
                    this.zeroBtnBlocked = false     //desbloqueia a tecla zero
                }

                this.display.append(symbol)         //adiciona o caracter no display
                this.opInProgress = false

                if(v.id == R.id.dot_btn)            //bloqueia a tecla "dot_btn" se o ponto já foi adicionado no display
                    this.dotBtnBlocked = true

            
            }else if(v.id != R.id.dot_btn){
                //Este bloco é executado caso a tecla pressionada for um operador    
                if(this.opInProgress) {
                    /*Este bloco é executado quando a operação está em progresso, ou seja,
                    * quando os dois operandos ainda não foram inseridos.*/
                    if(v.id == R.id.equal_btn){
                        //adiciona o sinal de igual no histórico
                        this.history.append(this.display.text.toString() + symbol)
                        this.secondOp = this.display.text.toString().toDouble()
                        this.opReady = true
                    }else{
                        //permite alterar o operador enquanto a operação está em andamento.
                        this.history.text = this.history.text.substring(0, this.history.text.lastIndex)
                        this.history.append(symbol)
                        this.operator = symbol
                    }
                }else {
                    //adiciona o operador no histórico
                    this.setOperands()          //configura os operandos
                    this.history.append(this.display.text.toString() + symbol)
                    this.opInProgress = true    //permite que o operador seja alterado
                    this.resultMode = true
                    this.opReady = true         //permite que o display mostre o resultado da operação
                    this.dotBtnBlocked = false
                }
            }

            if(this.resultMode && this.opReady){
                this.showResult(symbol)
                if(v.id == R.id.equal_btn)
                    this.opFinished = true
            }
        }else{
            //Caso a tecla do número zero seja pressionada
            if(!this.zeroBtnBlocked) {
                if(this.resultMode){
                    //sai do modo resultado, se resultMode == true
                    this.display.text = ""
                    this.zeroBtnBlocked = true
                    this.resultMode = false
                    this.opInProgress = false
                }
                //adiciona o número zero no display
                display.append(this.getSymbolById(R.id.zero_btn))
            }
        }
    }


    private fun setOperands(){
        if(this.result == null)
            this.firstOp = this.display.text.toString().toDouble()
        else{
            this.firstOp = this.result.toString().toDouble()
            this.secondOp = this.display.text.toString().toDouble()
        }
    }


    private fun showResult(symbol: String){
        this.display.text = this.getResult()
        this.operator = symbol
        this.opReady = false
    }


    private fun getResult(): String{
        if(this.result == null){
            this.result = this.firstOp.toString()
        }else if(this.operator != null){
            when(this.operator){
                "+" -> this.result = (this.firstOp + this.secondOp).toString()
                "-" -> this.result = (this.firstOp - this.secondOp).toString()
                "*" -> this.result = (this.firstOp * this.secondOp).toString()
                "/" -> {
                    if(this.secondOp != 0.0){
                        this.result = (this.firstOp / this.secondOp).toString()
                    }else{
                        this.result = "Infinity"
                        this.opFinished = true
                    }
                }
            }

        }
        return this.result!!
    }


    private fun getSymbolById(id: Int): String{
        return when (id) {
            R.id.zero_btn -> "0"
            R.id.one_btn -> "1"
            R.id.two_btn ->  "2"
            R.id.three_btn -> "3"
            R.id.four_btn -> "4"
            R.id.five_btn -> "5"
            R.id.six_btn -> "6"
            R.id.seven_btn -> "7"
            R.id.eight_btn -> "8"
            R.id.nine_btn -> "9"
            R.id.plus_btn -> "+"
            R.id.minus_btn -> "-"
            R.id.times_btn -> "*"
            R.id.division_btn -> "/"
            R.id.equal_btn -> "="
            R.id.dot_btn -> {
                return if(this.resultMode) {
                    "0."
                }else
                    "."
            }
            else -> {
                ""
            }
        }
    }


    fun delete(v: View){
        if(this.display.text.isNotEmpty() && !this.resultMode){
            if(this.display.text[this.display.text.lastIndex].toString() == ".") {
                this.dotBtnBlocked = false
            }
            this.display.text = this.display.text.substring(0, this.display.text.lastIndex)

            if(this.display.text.isEmpty() || this.display.text.toString() == "0"){
                this.resetVariables()
            }
        }
    }


    private fun clear(){
        this.history.text = ""
        this.opInProgress = false
        this.newOperation()
    }


    fun clearDisplay(v: View){
        if(this.opFinished) {
            this.clear()
        }else
            this.resetVariables()

    }


    private fun newOperation(){
        this.resetVariables()
        this.firstOp = 0.0
        this.secondOp = 0.0
        this.result = null
    }


    private fun resetVariables(){
        this.dotBtnBlocked = false
        this.zeroBtnBlocked = true
        this.display.text = "0"
        this.resultMode = true
        this.opFinished = false
        this.opReady = false
    }


}